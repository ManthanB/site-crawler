<?php

require "./Excel/PHPExcel.php";
require "./Excel/PHPExcel/IOFactory.php";

$allUrls = [];

function crawl_page($url, $depth = 5, &$all)
{
    static $seen = array();
    if (isset($seen[$url]) || $depth === 0) {
        return;
    }

    $seen[$url] = true;

    $dom = new DOMDocument('1.0');
    @$dom->loadHTMLFile($url);

    $anchors = $dom->getElementsByTagName('a');
    foreach ($anchors as $element) {
        $href = $element->getAttribute('href');
        if( 0 !== strpos($href, "javascript") 
            && 0 !== strpos($href, "http") 
            && 0 !== strpos($href, "tel") 
            && 0 !== strpos($href, "mail") 
            && 0 !== strpos($href, "#") 
            && 0 !== strpos($href, "' + item.url_point + '")
            ){
            $path = '/' . ltrim($href, '/');
            if (extension_loaded('http')) {
                $href = http_build_url($url, array('path' => $path));
            } else {
                $parts = parse_url($url);
                $href = $parts['scheme'] . '://';
                if (isset($parts['user']) && isset($parts['pass'])) {
                    $href .= $parts['user'] . ':' . $parts['pass'] . '@';
                }
                $href .= $parts['host'];
                if (isset($parts['port'])) {
                    $href .= ':' . $parts['port'];
                }
                $href .= $path;
            }
            if(strpos($href, "-detail")){
                
                $all[$url] = $href;    
                // if(strpos($url, "-detail")){
                // }
            }
            crawl_page($href, $depth - 1, $all);
        }
    }
    echo "URL: ",$url.PHP_EOL;
}
crawl_page("http://google.com", 2  , $allUrls);

$excel = new PHPExcel();
$excel->getProperties()->setCreator("Site URL Crawler")
                         ->setLastModifiedBy("Site URL Crawler")
                         ->setTitle("")
                         ->setSubject("")
                         ->setDescription("")
                         ->setKeywords("")
                         ->setCategory("");
$excel->setActiveSheetIndex(0);
$excel->getActiveSheet()
            ->getStyle('A1:B1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FF808080');
$excel->setActiveSheetIndex(0)->setCellValue('A1', 'PARENT');
$excel->setActiveSheetIndex(0)->setCellValue('B1', 'URL');
$id=2;
foreach($allUrls as $allKey => $allValue){
    $excel->setActiveSheetIndex(0)->setCellValue("A$id", $allKey);
    $excel->setActiveSheetIndex(0)->setCellValue("B$id", $allValue);
    $id++;
}

$excel->getActiveSheet()->setTitle('Simple');
$excel->setActiveSheetIndex(0);
ob_start();
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
$path = "./Test.xlsx";
$objWriter->save("php://output");
$xlsData = ob_get_contents();
ob_end_clean();
$file = fopen($path, "wb");
fwrite($file, $xlsData);
fclose($file);