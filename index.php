#!/usr/bin/php
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

$handle = fopen ("php://stdin","r");



require "./vendor/autoload.php";

$crawler = new \ManthanB\SiteCrawler\UrlCrawler();
$settings = new \ManthanB\SiteCrawler\UrlCrawlerSettings();

echo PHP_EOL."Site URL: ";
$url = trim(fgets($handle));
if($url==''){
	echo "Site URL is mendatory!".PHP_EOL;
	die;
}
$settings->setUrl($url);

echo PHP_EOL."Depth (leave blank for default): ";
$depth = trim(fgets($handle));
if($depth!=''){
	$settings->setDepth($depth);	
}

echo PHP_EOL."Excel File name (leave blank for default): ";
$excelFileName = trim(fgets($handle));
if($excelFileName!=''){
	$settings->setExcelFileName($excelFileName);
}

$crawler->setSettings($settings);
$crawler->crawlUrl();
?>