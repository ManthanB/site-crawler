<?php
namespace ManthanB\SiteCrawler;

class UrlCrawlerSettings {
	/**
	 * 
	 * @var string 
	 * main domain to crawl
	 */ 
	private $url;

	/**
	 * 
	 * @var int 
	 * depth for crawle urls 
	 */
	private $depth;

	private $excelFileName;

	/**
	 * 
	 * @var array 
	 * to avoid urls starting from it
	 */
	private $excludeUrlSchemes;

	function __construct(){
		$this->setExcludeUrlSchemes(["javascript", "http", "https", "tel", "mail"]);
		$this->setDepth(2);
		$this->setExcelFileName("Demo.xlsx");
	}

	public function setUrl($url){
		$this->url = $url;
	}

	public function getUrl(){
		return $this->url;
	}

	public function setDepth($depth){
		$this->depth = $depth;
	}

	public function getDepth(){
		return $this->depth;
	}

	public function setExcludeUrlSchemes($excludeUrlSchemes){
		$this->excludeUrlSchemes = $excludeUrlSchemes;
	}

	public function getExcludeUrlSchemes(){
		return $this->excludeUrlSchemes;
	}

	public function setExcelFileName($excelFileName){
		$this->excelFileName = $excelFileName;
	}

	public function getExcelFileName(){
		return $this->excelFileName;
	}

}
?>