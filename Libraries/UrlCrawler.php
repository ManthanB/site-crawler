<?php
namespace ManthanB\SiteCrawler;

use \PHPExcel;

class UrlCrawler {

	private $crawledUrls = [];
	private $tempurls = [];

	private $domDocument;

	private $client;

	function __construct(){
		$this->domDocument = new \DOMDocument('1.0');
		$this->client = new \GuzzleHttp\Client(['allow_redirects' => false, 'exceptions' => FALSE]);
	}

	public function setSettings(\ManthanB\SiteCrawler\UrlCrawlerSettings $settings){
		$this->settings = $settings;
	}

	public function crawlUrl($url = null, $depth=null){
		$url = $this->settings->getUrl();
		$depth = $this->settings->getDepth();
	    $excelGenerator = new \ManthanB\SiteCrawler\ExcelGenerator();

	    $this->makeRequest($url);
	
		$excelGenerator->setSettings($this->settings);
		$excelGenerator->setCrawledUrls($this->crawledUrls);
		$excelGenerator->generateExcel();
	}

	public function makeRequest($url, $depth = null){
		if($depth == null){
			$depth = $this->settings->getDepth();
		}
		if (in_array($url, $this->tempurls) || $depth === 0) {
	        return;
	    }
		$res = $this->client->request('GET', $url);
		echo "--------------------------------------------".PHP_EOL;
		echo "- ".count($this->tempurls).PHP_EOL;
		echo "- URL: $url".PHP_EOL;
		echo "- Status: ".$res->getStatusCode().PHP_EOL;
		echo "--------------------------------------------".PHP_EOL;
		$crawledUrl = new \ManthanB\SiteCrawler\CrawledUrl();

		$crawledUrl->setStatus($res->getStatusCode());
		$crawledUrl->setContentType($res->getHeaderLine('content-type'));
		$crawledUrl->setUrl($url);
		$this->tempurls[] = $url;
		$this->crawledUrls[] = $crawledUrl;

		libxml_use_internal_errors(true);
		if($res->getStatusCode() != 404 && $res->getBody() != ''){
			
			$this->domDocument->loadHTML($res->getBody());
			libxml_use_internal_errors(false);
			$anchors = $this->domDocument->getElementsByTagName('a');
			
			foreach($anchors as $element){
				$href = $element->getAttribute('href');
				// echo "<pre>";print_r($href);
				if($this->checkUrl($href)){

					$path = '/' . ltrim($href, '/');
		            if (extension_loaded('http')) {
		                $href = http_build_url($url, array('path' => $path));
		            } else {
		                $parts = parse_url($url);
		                $href = $parts['scheme'] . '://';
		                if (isset($parts['user']) && isset($parts['pass'])) {
		                    $href .= $parts['user'] . ':' . $parts['pass'] . '@';
		                }
		                $href .= $parts['host'];
		                if (isset($parts['port'])) {
		                    $href .= ':' . $parts['port'];
		                }
		                $href .= $path;
		            }
		            // echo $href.PHP_EOL;
					
					$this->makeRequest($href, $depth-1);
				}
			}
		}
		return;
		// die;
	}

	public function checkUrl($url){
		$schemesToExclude = implode("|", $this->settings->getExcludeUrlSchemes());
		if(!preg_match("/^($schemesToExclude)/", $url)){
			return true;
		}
		return false;
	}
}

?>