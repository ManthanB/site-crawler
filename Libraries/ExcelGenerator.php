<?php
namespace ManthanB\SiteCrawler;

class ExcelGenerator{

	private $settings;

	private $crawledUrls;

	function __construct(){
		$this->excel = new \PHPExcel();
	}

	public function setSettings(\ManthanB\SiteCrawler\UrlCrawlerSettings $settings){
		$this->settings = $settings;
	}

	public function setCrawledUrls($crawledUrls){
		$this->crawledUrls = $crawledUrls;
	}

	public function generateExcel(){
		$this->excel->getProperties()->setCreator("Site URL Crawler")
		                         ->setLastModifiedBy("Site URL Crawler")
		                         ->setTitle($this->settings->getExcelFileName())
		                         ->setSubject($this->settings->getExcelFileName())
		                         ->setDescription("")
		                         ->setKeywords("")
		                         ->setCategory("");
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()
		            ->getStyle('A1:B1')
		            ->getFill()
		            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
		            ->getStartColor()
		            ->setARGB('FF808080');
		$this->excel->setActiveSheetIndex(0)->setCellValue('A1', 'URL');
		$this->excel->setActiveSheetIndex(0)->setCellValue('B1', 'STATUS');
		$this->excel->setActiveSheetIndex(0)->setCellValue('C1', 'Content Type');
		$id=2;
		foreach($this->crawledUrls as $url){
		    $this->excel->setActiveSheetIndex(0)->setCellValue("A$id", $url->getUrl());
		    $this->excel->setActiveSheetIndex(0)->setCellValue("B$id", $url->getStatus());
		    $this->excel->setActiveSheetIndex(0)->setCellValue("C$id", $url->getContentType());
		    $id++;
		}

		$this->excel->getActiveSheet()->setTitle('Links');
		$this->excel->setActiveSheetIndex(0);
		ob_start();
		$objWriter = \PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		$path = "./".$this->settings->getExcelFileName();
		$objWriter->save("php://output");
		$xlsData = ob_get_contents();
		ob_end_clean();
		$file = fopen($path, "wb");
		fwrite($file, $xlsData);
		fclose($file);
		return true;
	}


}

?>