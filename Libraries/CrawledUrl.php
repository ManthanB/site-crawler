<?php
namespace ManthanB\SiteCrawler;

class CrawledUrl{

	private $status;

	private $contentType;

	private $url;

	public function setStatus($status){
		$this->status = $status;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setContentType($contentType){
		$this->contentType = $contentType;
	}

	public function getContentType(){
		return $this->contentType;
	}

	public function setUrl($url){
		$this->url = $url;
	}

	public function getUrl(){
		return $this->url;
	}

}


?>